package cat.inspiracio;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class RomanTest {
	
	Map<Integer,String>m=new HashMap<Integer,String>();
	{
		m.put(1,"I");
		m.put(2,"II");
		m.put(3,"III");
		m.put(4,"IV");
		m.put(5,"V");
		m.put(6,"VI");
		m.put(7,"VII");
		m.put(8,"VIII");
		m.put(9, "IX");
		m.put(10, "X");
		m.put(11, "XI");
		m.put(12, "XII");
		m.put(13, "XIII");
		m.put(14, "XIV");
		m.put(15, "XV");
		m.put(16, "XVI");
		m.put(17, "XVII");
		m.put(18, "XVIII");
		m.put(19, "XIX");
		m.put(20, "XX");
		m.put(21, "XXI");
		m.put(22, "XXII");
		m.put(23, "XXIII");
		m.put(24, "XXIV");
		m.put(25, "XXV");
		m.put(26, "XXVI");
		m.put(27, "XXVII");
		m.put(28, "XXVIII");
		m.put(29, "XXIX");
		m.put(30, "XXX");
		m.put(31, "XXXI");
		m.put(32, "XXXII");
		
		m.put(39, "XXXIX");
		m.put(40, "XL");
		m.put(41, "XLI");
		
		m.put(49, "XLIX");
		m.put(50, "L");
		m.put(51, "LI");
		
		m.put(99, "XCIX");
		m.put(100, "C");
		m.put(101, "CI");

		m.put(399, "CCCXCIX");
		m.put(400, "CD");
		m.put(401, "CDI");
		
		m.put(499, "CDXCIX");
		m.put(500, "D");
		m.put(501, "DI");

		m.put(899, "DCCCXCIX");
		m.put(900, "CM");
		
		m.put(999, "CMXCIX");
		m.put(1000, "M");
		
		m.put(1999, "MCMXCIX");
		m.put(2000, "MM");
		
		m.put(2999, "MMCMXCIX");
		m.put(3000, "MMM");
		
		m.put(3999, "MMMCMXCIX");
	}
	
	/** Format:
	 * new Roman(int).toString() */
	@Test public void format(){
		for(Map.Entry<Integer, String> e : m.entrySet()){
			int v=e.getKey();
			String s=e.getValue();
			Roman r=new Roman(v);
			assertEquals(s, r.toString());
		}
	}
	
	/** Parse:
	 * new Roman(String).intValue() */
	@Test public void parse(){
		for(Map.Entry<Integer, String> e : m.entrySet()){
			int v=e.getKey();
			String s=e.getValue();
			Roman r=new Roman(s);
			assertEquals(v, r.intValue());
		}
	}

}
