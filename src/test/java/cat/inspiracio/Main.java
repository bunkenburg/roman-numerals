package cat.inspiracio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class Main {

	private BufferedReader reader;
	private PrintStream out;

	private Main() throws UnsupportedEncodingException{
		reader=new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
		out=System.out;
	}
	
	/** Console application to convert numbers. 
	 * At each prompt, enter a Roman or standard number, or "q" to quit. 
	 * @throws IOException */
	public static void main(String[] args) throws IOException{
		Main m=new Main();
		m.run();
	}
	
	private void run() throws IOException{
		String in=prompt();
		while(!"q".equalsIgnoreCase(in)){
			
			//Is it Roman?
			try{
				Roman r=new Roman(in);
				out.println(r.intValue());
			}catch(IllegalArgumentException e){}
			
			//Is it standard?
			try{
				int i=Integer.parseInt(in);
				Roman r=new Roman(i);
				out.println(r);
			}
			catch(NumberFormatException e){}
			
			in=prompt();
		}

	}
	
	private String prompt() throws IOException{
		out.print("? ");
		return reader.readLine();
	}
	
}
