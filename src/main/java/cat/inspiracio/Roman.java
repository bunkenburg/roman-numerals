package cat.inspiracio;

public class Roman extends Number{
	private static final long serialVersionUID = 4755204994504689344L;

	public static final int MIN=1;
	public static final int MAX=3999;//MMMCMXCIX
	
	// state --------------------------
	
	private int value=0;
	
	// construction -------------------
	
	/** Parse a Roman numeral from a String.
	 * @throws NullPointerException The string is null.
	 * @throws IllegalArgumentException The string is not a valid Roman numeral. 
	 */
	public Roman(String s)throws IllegalArgumentException{
		if(s==null)throw new NullPointerException();
		
		value=0;
		int last=0;
		int length=s.length();
		for(int i=length-1; 0<=i; i--){
			char c=s.charAt(i);
			int v=value(c);
			if(v==0)
				throw new IllegalArgumentException(s);
			if(last==0)
				value = v;
			else if(last!=0 && v<last)
				value -= v;
			else if(last!=0 && last<=v)
				value += v;
			last=v;
		}
		
		//checking for valid Roman numeral:
		if(!s.equals(this.toString()))
			throw new IllegalArgumentException(s);
	}

	private int value(char c)throws IllegalArgumentException{
		switch(c){
		case 'I':return 1;
		case 'V':return 5;
		case 'X':return 10;
		case 'L':return 50;
		case 'C':return 100;
		case 'D':return 500;
		case 'M':return 1000;
		default: return 0;//bad value
		}
	}

	/** Make a Roman numeral from an int.
	 * @throws IllegalArgumentException if not 0 <= i <= 3999. */
	public Roman(int i)throws IllegalArgumentException{
		if(i<MIN)throw new IllegalArgumentException();
		if(MAX<i)throw new IllegalArgumentException();
		value=i;
	}
	
	// Number -------------------------
	
	@Override public double doubleValue(){return intValue();}
	@Override public float floatValue(){return intValue();}
	@Override public long longValue(){return intValue();}
	@Override public int intValue(){return value;}

	/** @return The number in Roman notation. */
	@Override public String toString(){return toString(value);}

	/** Formats an int as Roman numeral. */
	private String toString(int i){
		//Some better idea than recursion with string concatenation?
		if(1000<=i)
			return "M" + toString(i-1000);
		if(900<=i)
			return "CM" + toString(i-900);
		if(500<=i)
			return "D" + toString(i-500);
		if(400<=i)
			return "CD" + toString(i-400);
		if(100<=i)
			return "C" + toString(i-100);
		if(90<=i)
			return "XC" + toString(i-90);
		if(50<=i)
			return "L" + toString(i-50);
		if(40<=i)
			return "XL" + toString(i-40);
		if(10<=i)
			return "X" + toString(i-10);
		if(9<=i)
			return "IX" + toString(i-9);
		if(5<=i)
			return "V" + toString(i-5);
		if(4<=i)
			return "IV" + toString(i-4);
		if(1<=i)
			return "I" + toString(i-1);
		return "";
	}
}
